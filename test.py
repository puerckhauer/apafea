from apafea import run

if __name__ == '__main__':
    run(
        mode="KPM",
        # data="/absolute/path/to/directory/",
        mode_2="BiCoN",
        # data_2="/absolute/path/to/results.csv",
        gsea_dir="C:\\Users\\korbi\\OneDrive\\Desktop\\ey",
        GO=["bp",
            "cc",
            "mf",
            "pw"],
        PW=["KEGG",
            "Reactome",
            "WikiPathways",
            "MSigDB_C",
            "MSigDB_O_S"],
        all=False,
        cutoff=0.00005,
        font_size=10,
        edge_thicness=0.1,
    )
