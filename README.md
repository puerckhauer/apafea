# apafea

     ______     ______   ______     ______   ______     ______    
    /\  __ \   /\  == \ /\  __ \   /\  ___\ /\  ___\   /\  __ \   
    \ \  __ \  \ \  _-/ \ \  __ \  \ \  __\ \ \  __\   \ \  __ \  
     \ \_\ \_\  \ \_\    \ \_\ \_\  \ \_\    \ \_____\  \ \_\ \_\ 
      \/_/\/_/   \/_/     \/_/\/_/   \/_/     \/_____/   \/_/\/_/ 

* [Description](#description)
* [Installation](#installation)
* [Usage](#usage)
* [History](#history)

### Description
apafea is short for "alluvial plots as functional enrichment analysis".\
And it does just that. Input a set of gene clusters (supports KeyPathwayMiner & BiCoN) and apafea performs gene set enrichment analysis and shows an alluvial plot visualizing the results.\
For further Information check out [usage](#usage).
### Installation
Install the package with:
```pip install apafea```
#### Dependencies
* gseapy == 0.10.8
* plotly == 5.6.0
### Usage
###### python:
```python
from apafea import run

run(
    mode="KPM",
    data="/absolute/path/to/directory/",
    mode_2="BiCoN",
    data_2="/absolute/path/to/results.csv",
    gsea_dir="/absolute/path/to/gsea/directory/",
    GO=["bp",
        "cc",
        "mf",
        "pw"],
    PW=["KEGG",
        "Reactome",
        "WikiPathways",
        "MSigDB_C",
        "MSigDB_O_S"],
    all=False,
    cutoff=0.05,
)
```
###### explanation:
* "mode" - defines the type of input.
  * "KPM" - give the path to your KeyPathwayMiner output directory under "data"
  * "BiCoN" - give the path to your BiCoN result .csv file under "data"
  * "custom" - give the path to your directory under "data" containing 1 cluster per file like this:
  ```
  gene1
  gene2
  ...
  ```
+ "mode_2" - defines the type of input for cluster comparison analysis.
  * "KPM" - same as above
  * "BiCoN" - same as above
  * "custom" - same as above
  * None / not set - cluster comparison analysis will not be performed
* "gsea_dir" - give the path to a directory to be used as gene set enrichment analysis output directory (default: /tmp)
* "GO" - list of GO-Terms enrichment analysis is being performed for
  * "bp" - Biological process
  * "cc" - Cellular Component
  * "mf" - molecular Function
  * "pw" - KEGG Pathways
  * None / [] / not set - analysis will not be performed
* "PW" - list of pathway databases enrichment analysis is being performed for
  * "KEGG" - KEGG_2016
  * "Reactome" - Reactome_2016
  * "WikiPathways" - WikiPathways_2016
  * "MSigDB_C" - MSigDB_Computational
  * "MSigDB_O_S" - MSigDB_Oncogenic_Signatures
  * None / [] / not set - analysis will not be performed
* "all" - associates all its terms to a gene (default: False)
* "cutoff" - defines the cutoff for gene set enrichment analysis (default: 0.05)
### History
* 0.0.0 - initial commit
* 0.0.1 - first official release
* 0.0.2 - added MANIFEST.in and changed relative path handling for sample data
* 0.0.3 - fixed bug with relative path handling for sample data
* 0.0.4 - fixed bug with relative path handling for sample data
* 0.0.5 - fixed bug with relative path handling for sample data
* 0.0.6 - fixed bug with relative path handling for sample data
* 0.0.7 - added dependencies
* 0.0.8 - added adjustable font size feature
* 0.0.9 - added adjustable edge thicness feature
