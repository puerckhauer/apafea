from setuptools import setup, find_packages

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
    name="apafea",
    author="Korbinian Pürckhauer",
    author_email="korbinian@puerckhauer.net",
    maintainer="Korbinian Pürckhauer",
    maintainer_email="korbinian@puerckhauer.net",
    version="0.0.9",
    url="https://gitlab2.cip.ifi.lmu.de/puerckhauer/apafea",
    download_url="https://gitlab2.cip.ifi.lmu.de/puerckhauer/apafea",
    keywords=["gene", "enrichment", "alluvial", "sankey", "bioinformatics"],
    license="MIT",
    description="visualizes genes functional enrichment",
    long_description=long_description,
    long_description_content_type="text/markdown",
    project_urls={
        "Bug tracker": "https://gitlab2.cip.ifi.lmu.de/puerckhauer/apafea",
    },
    python_requires=">=3.10",
    packages=find_packages(),
    include_package_data=True,
    package_data={
        "samples": ["sample_data/*"],
    },
    insall_requires=[
        "gseapy==0.10.8",
        "plotly==5.6.0",
        "setuptools==57.0.0",
    ],
    classifiers=[
        "Operating System :: Microsoft :: Windows",
        "Operating System :: MacOS",
        "Programming Language :: Python",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
    ],
    entry_points={
        "console_scripts": [
            "main = apafea.__main__:main",
        ]
    },
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
)
